import argparse
import json
from random import shuffle
import pandas as pd
import tflearn
import tensorflow as tf

def read_json_trainer(files_name):
	nuc_data= []
	amino_data = []
	i = 0 #represents the block
	for f in files_name:
		
		with open(f) as json_file:  
			file = json.load(json_file)
	
		nuc_data.append([])
		amino_data.append([])
	
		j=0
		for data in file:
			for pid in data:
				nuc_data[i].append([])
				amino_data[i].append([])

				nuc_data[i][j].append(pid)
				nuc_data[i][j].append(data[pid]['features']['NU'])
				nuc_data[i][j].append(data[pid]['label'])
				nuc_data[i][j].append(data[pid]['type'])

				amino_data[i][j].append(pid)
				amino_data[i][j].append(data[pid]['features']['AA'])
				amino_data[i][j].append(data[pid]['label'])
				amino_data[i][j].append(data[pid]['type'])
		
			j+= 1        
		i += 1 

	feature_names_AA = data[pid]['features_names']['AA'] 
	feature_names_NU = data[pid]['features_names']['NU']

	return nuc_data, amino_data,feature_names_AA,feature_names_NU


#get all the data from a specific order
def get_data_order(data, order_name):
    new_data = data
    for i in range(len(data)):

        if (new_data[i][2] == order_name):
            new_data[i][2] = [1,0]
        else:
            new_data[i][2] = [0,1]

    return new_data

def merge_amino_nuc(amino, nuc):
    final_df = nuc
    return final_df.join(amino.iloc[:, :2].set_index('pid'), on='pid', how='inner')

def get_train_test_data(nu_list,aa_list):
    index_pid = 0
    index_x = 1
    index_y = 2
    index_type = 3
    train_aa_x = [] 
    train_nu_x = []
    data_y_train = [] 
    test_aa_x = []
    test_nu_x = []
    data_y_test = []
    pids_test = []
    
    for i in range(len(nu_list)):
        if ((nu_list[i][index_type]) == 'train'):
            train_nu_x.append(nu_list[i][index_x])
            train_aa_x.append(aa_list[i][index_x])
            data_y_train.append(nu_list[i][index_y])
        else:
            test_nu_x.append(nu_list[i][index_x])
            test_aa_x.append(aa_list[i][index_x])
            data_y_test.append(nu_list[i][index_y])
            pids_test.append(nu_list[i][index_pid])
            
            
    return train_aa_x, train_nu_x, data_y_train, test_aa_x, test_nu_x, data_y_test, pids_test

def build_DNN(train_x,train_y):
	tf.reset_default_graph()
	# Build neural network - input data shape, number of words in vocabulary (size of first array element). 
	net = tflearn.input_data(shape=[None, len(train_x[0])])
	# Two fully connected layers with 8 hidden units/neurons - optimal for this task
	net = tflearn.fully_connected(net, 4)
	net = tflearn.fully_connected(net, 4,activation='softmax')
	# number of intents, columns in the matrix train_y
	#net = tflearn.fully_connected(net, len(train_y[0]), activation='softmax')
	# regression to find best parameters, during training
	net = tflearn.regression(net)

	# Define Deep Neural Network model and setup tensorboard
	model = tflearn.DNN(net, tensorboard_dir='tflearn_sequence_logs')
	return model

def build_DNN_Arch1(aa_length, nu_length, y_length):
    tf.reset_default_graph()
    # Build neural network - input data shape, number of words in vocabulary (size of first array element). 
    net_aa = tflearn.input_data(shape=[None, aa_length], name="InputData0")
    net_nu = tflearn.input_data(shape=[None, nu_length], name="InputData1")
    # Two fully connected layers with 8 hidden units/neurons
    net_aa = tflearn.fully_connected(net_aa, 4)
    net_nu = tflearn.fully_connected(net_nu, 4)
    # number of intents, columns in the matrix train_y
    net = tflearn.merge_outputs([net_aa, net_nu])
    net = tflearn.fully_connected(net, 4)
    net = tflearn.fully_connected(net, y_length, activation='sigmoid')
    # regression to find best parameters, during training
    net = tflearn.regression(net, optimizer='adam')

    # Define Deep Neural Network model and setup tensorboard
    model = tflearn.DNN(net, tensorboard_dir='tflearn_arch1_logs', tensorboard_verbose=3)
    return model

def build_DNN_Arch2(aa_length, nu_length, y_length):
    tf.reset_default_graph()
    # Build neural network - input data shape, number of words in vocabulary (size of first array element). 
    net_aa = tflearn.input_data(shape=[None, aa_length], name="InputData0")
    net_nu = tflearn.input_data(shape=[None, nu_length], name="InputData1")
    # Two fully connected layers with 8 hidden units/neurons
#     net_aa = tflearn.fully_connected(net_aa, 4)
    net_nu = tflearn.fully_connected(net_nu, 4)
    # number of intents, columns in the matrix train_y
    net = tflearn.merge_outputs([net_aa, net_nu])
    net = tflearn.fully_connected(net, 4)
    net = tflearn.fully_connected(net, y_length, activation='sigmoid')
    # regression to find best parameters, during training
    net = tflearn.regression(net, optimizer='adam')

    # Define Deep Neural Network model and setup tensorboard
    model = tflearn.DNN(net, tensorboard_dir='tflearn_arch2_logs', tensorboard_verbose=3)
    return model

def train_DNN(model, train_x,train_y,classes):
    # Start training (apply gradient descent algorithm)
    # n_epoch - number of epoch to run
    # Batch size defines number of samples that going to be propagated through the network.
    model.fit(train_x, train_y, n_epoch=5, batch_size=5, show_metric=True)
    model.save('model/'+classes[0]+'_'+classes[1]+'.tflearn')

def train_DNN_arch12(model, train_aa, train_nu, train_y,name,grp):
    # Start training (apply gradient descent algorithm)
    # n_epoch - number of epoch to run
    # Batch size defines number of samples that going to be propagated through the network.
    model.fit({"InputData0":train_aa,"InputData1":train_nu}, train_y, n_epoch=5, batch_size=5, show_metric=True)
    model_name = 'model/'+name+'_'+grp+'.tflearn'
    model.save(model_name)
    return model_name

def main():

	ap = argparse.ArgumentParser()
	ap.add_argument('-f','--features_file', nargs='+', help="Features Files", required=True) # Use like: python train.py -f features_file1 features_file2 features_file3 features_file4	
	ap.add_argument("-t","--label_target",required=True,help="Label target")
	ap.add_argument("-o","--output_name",required=True,help="Output name")

	args = vars(ap.parse_args())

	files_name = args["features_file"]
	cls_target = args["label_target"]
	output_name = args["output_name"]


	classes = [cls_target, 'not_'+cls_target]

	#tt_dict = {}

	#Read json file
	nuc_data_all, amino_data_all, _, _ = read_json_trainer(files_name)

	for i in range(len(files_name)):

		index = str(i)
		nuc_x_name = 'ndata_x%s' % (index)
		amino_x_name = 'adata_x%s' % (index)
		data_y_name = 'data_y%s' % (index)
		#tt_dict[i] = {}

		nuc_data = nuc_data_all[i]
		nuc_data = get_data_order(nuc_data, classes[0])
		nuc_data = pd.DataFrame(data = nuc_data, columns=['pid', nuc_x_name, data_y_name,'type'])

		amino_data = amino_data_all[i]
		amino_data = get_data_order(amino_data, classes[0])
		amino_data = [item[:3] for item in amino_data]
		#print(amino_data[0])
		amino_data = pd.DataFrame(data = amino_data, columns=['pid', amino_x_name, data_y_name])
		

		final_df = merge_amino_nuc(amino_data, nuc_data)
		nu_list = final_df[['pid',nuc_x_name,data_y_name,'type']].values.tolist()
		aa_list = final_df[['pid',amino_x_name,data_y_name,'type']].values.tolist()
		
		train_aa_x, train_nu_x, train_y, _, _, _, _ = get_train_test_data(nu_list,aa_list)
		#tt_dict[i]['train_aa_x'] = train_aa_x
		#tt_dict[i]['train_nu_x'] = train_nu_x
		#tt_dict[i]['train_y'] = train_y

		model = build_DNN_Arch1(len(train_aa_x[0]), len(train_nu_x[0]), len(train_y[0]))
		train_DNN_arch12(model, train_aa_x, train_nu_x,train_y,output_name,str(i))

main()