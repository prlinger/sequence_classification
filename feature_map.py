import os, sys, json
import numpy as np
from Bio import SeqIO
from Bio.SeqIO.FastaIO import SimpleFastaParser
import pickle
from itertools import product
import random
import argparse

def batch_iterator(iterator, batch_size):
    """Returns lists of length batch_size.

    This can be used on any iterator, for example to batch up
    SeqRecord objects from Bio.SeqIO.parse(...), or to batch
    Alignment objects from Bio.AlignIO.parse(...), or simply
    lines from a file handle.

    This is a generator function, and it returns lists of the
    entries from the supplied iterator.  Each list will have
    batch_size entries, although the final list may be shorter.
    """
    entry = True  # Make sure we loop once
    while entry:
        batch = []
        while len(batch) < batch_size:
            try:
                entry = next(iterator)
            except StopIteration:
                entry = None
            if entry is None:
                # End of file
                break
            batch.append(entry)
        if batch:
            yield batch

def save_as_json(obj, filename):
	with open(filename, 'w') as fp:
	    json.dump(obj, fp)

def get_key_product(k_value, fastaFileList, directory, start_position, end_position):
	# fastaFileList = list_files(directory)
	k_value = int(k_value)
	folder_name = str()
	k_mer_list = []
	
	for fastaFile in fastaFileList:
		order = fastaFile.replace('.txt', '').replace('.fasta', '')
		folder_name = order + "_amino"
		file = 'data/'+directory+'/'+fastaFile
		
		record_iter = SeqIO.parse(open(file), "fasta")
		for i, batch in enumerate(batch_iterator(record_iter, 50)):
			# if i < 5:
			filename = "data/%s/group_%i.fasta" % (folder_name, i + 1)
			with open(filename) as in_handle:
				for title, seq in SimpleFastaParser(in_handle):
					seq = seq.upper()
					seq = seq[start_position:end_position]
					for index in range(0, len(seq)-k_value+1):
						k_mer = seq[index:index+k_value]
						if ('-' not in k_mer) and ('*' not in k_mer) and ('X' not in k_mer): 
							if k_mer not in k_mer_list:
								k_mer_list.append(k_mer)
	return k_mer_list

def get_frequency(data):
	frequency = {}
	data_sum = sum(data.values())
	for k_mer in data:
		frequency[k_mer] = float("{0:.10f}".format(float(data[k_mer]) / float(data_sum)))
	return frequency


def get_k_mer_list(k_value, fastaFileList, keys, directory, start_position, end_position, features):
	k_value = int(k_value)
	folder_name = str()
	k_mer_result = []
	for fastaFile in fastaFileList:
		order = fastaFile.replace('.txt', '').replace('.fasta', '')
		if directory == "nuc":
			folder_name = order
		elif directory == "amino":
			folder_name = order + "_amino"		
		file = 'data/' + directory+'/'+fastaFile
		record_iter = SeqIO.parse(open(file), "fasta")
		for i, batch in enumerate(batch_iterator(record_iter, 50)):
			# if i < 5:
			filename = "data/%s/group_%i.fasta" % (folder_name, i + 1)
			with open(filename) as in_handle:
				for title, seq in SimpleFastaParser(in_handle):
					seq = seq.upper()
					seq = seq[start_position:end_position]
					missing_seq = seq.count('-') + seq.count('X') + seq.count('N')
					each_seq = {}
					if missing_seq < len(seq) * 0.1 and len(seq) == (end_position-start_position):
						title = title[:title.index("|")]
						each_seq['pid'] = title
						each_seq['label'] = order
						count_result = {}
						count_result[title] = dict.fromkeys(keys)
						for index in range(0, len(seq)-k_value+1):
							k_mer = seq[index:index+k_value]
							if k_mer in count_result[title]:
								if count_result[title][k_mer] is None:
									count_result[title][k_mer] = 1
								else:
									count_result[title][k_mer] += 1
						for key in count_result[title]:
							if count_result[title][key] is None:
								count_result[title][key] = 0
						frequency_data = get_frequency(count_result[title])
						
						each_seq['features'] = {}
						each_seq['features_names'] = {}
						mapped_data = []
						for feature in features:
							if feature in frequency_data:
								mapped_data.append(frequency_data[feature])
							else:
								mapped_data.append(0.0)
						if directory == "nuc":
							each_seq['features']['NU'] = mapped_data
							each_seq['features_names']['NU'] = features
						elif directory == 'amino':
							each_seq['features']['AA'] = mapped_data
							each_seq['features_names']['AA'] = features
					if len(each_seq) != 0:
						k_mer_result.append(each_seq)
		# print(k_mer_result)
	return k_mer_result


if __name__ == '__main__':

	ap = argparse.ArgumentParser()
	ap.add_argument("-ak","--amino_acid_k_value",required=False,help="k value for amino acid")
	ap.add_argument("-nk","--nucleotide_k_value",required=False,help="k value for nucleotide")
	ap.add_argument("-f","--files",required=True,help="files that need to be executed")
	ap.add_argument("-abl","--amino_acid_block_length",required=False,help="the length of each block for amino acid sequence")
	ap.add_argument("-nbl","--nucleotide_block_length",required=False,help="the length of each block for nucleotide sequence")
	ap.add_argument("-flf", "--feature_list_file", required=True,help="the feature list file")

	args = vars(ap.parse_args())

	amino_k_values = args["amino_acid_k_value"]
	nucleo_k_values = args["nucleotide_k_value"]
	files = args["files"]
	files = files.split("-")
	feature_list_file = args["feature_list_file"]
	amino_block_length = args["amino_acid_block_length"]
	nucleo_block_length = args["nucleotide_block_length"]

	if amino_block_length != None:
		amino_block_length = amino_block_length.split("-")
		amino_block_length = [int(i) for i in amino_block_length]
	if nucleo_block_length != None:
		nucleo_block_length = nucleo_block_length.split("-")
		nucleo_block_length = [int(i) for i in nucleo_block_length]

	sequence_directory = "nuc"
	amino_directory = "amino"
	# features_list = {'amino': {0: ['HP', 'VV', 'CE', 'CN', 'KC', 'WI', 'EA', 'TC', 'CP', 'MW', 'GD', 'NP', 'FV', 'CG', 'RS', 'GH', 'GR', 'DF', 'AE', 'NV', 'SW', 'YH', 'SC', 'KW', 'QR', 'CD', 'SE', 'RA', 'RP'], 1: ['EE', 'DR', 'SH', 'GC', 'NY', 'YW', 'RP', 'KN', 'HI', 'AY', 'VE', 'KQ', 'CP', 'VH', 'NA', 'FV', 'PF', 'AE', 'NV', 'DE', 'YH', 'FQ', 'SC', 'WA', 'ET', 'RA', 'TE', 'YL', 'EP'], 2: ['NH', 'IC', 'QM', 'DK', 'HI', 'QV', 'NT', 'VE', 'EA', 'EN', 'IV', 'PE', 'RS', 'YS', 'WM', 'MY', 'AE', 'NV', 'RE', 'YH', 'IP', 'NS', 'HK', 'HE', 'RF', 'WV', 'CN', 'NQ', 'AH', 'QG', 'ES', 'CG', 'MF', 'ET', 'PC'], 3: ['HK', 'MC', 'HP', 'NH', 'RK', 'IC', 'RM', 'QE', 'TQ', 'HI', 'NC', 'AH', 'SQ', 'RQ', 'HQ', 'CP', 'SA', 'MH', 'AN', 'CT', 'YK', 'AM', 'DF', 'RE', 'YH', 'HT', 'YA', 'AK', 'RA', 'HH'], 4: ['VG', 'MC', 'IC', 'YV', 'PW', 'HN', 'MH', 'TG', 'ER', 'PD', 'GI', 'GN', 'NV', 'NN', 'RE', 'LC', 'PT', 'RH', 'HH', 'HK', 'HE', 'YW', 'RR', 'QG', 'IK', 'CT', 'YK', 'PQ', 'SW', 'RC', 'NG', 'EG', 'AP', 'RA', 'FP', 'RP'], 5: ['NY', 'YN', 'FA', 'RV', 'SA', 'FN', 'MA', 'MD', 'GI', 'WM', 'DF', 'NV', 'IQ', 'FQ', 'LC', 'AV', 'VP', 'VV', 'VN', 'TF', 'AY', 'TC', 'NE', 'VA', 'YD', 'KP', 'NF', 'MK', 'EF', 'TT', 'QD', 'RY', 'PC', 'YL']}, 'nuc': {0: ['GGA', 'ATGTG', 'GTG', 'GACAC', 'GGTAG', 'TGT', 'TCAGC', 'AGGGC', 'ACT', 'AAACC', 'GCCGA', 'GCAGA', 'AATGT', 'CGTTT', 'AACCC', 'CCG', 'TAT', 'AGACC', 'CCTGT', 'CCGAG', 'GCCAT', 'GCTGA', 'ACGAG', 'AGCCG', 'TCTGC', 'TCT', 'GCC', 'CGTGC', 'AAG', 'GCATC', 'CAA', 'ACGTT', 'ATC', 'CGAGC', 'CCGAC', 'AGC', 'ATG', 'TAG', 'ACG', 'CTG', 'ATA', 'ATGTA', 'AGCTG', 'GCT', 'CTGAA', 'AGGTA', 'CTC', 'GGCCA', 'AGT', 'CATGT', 'AGGAC', 'GCCAC', 'GAG', 'CGG', 'TGA', 'AGGTC', 'ACGTA', 'CCC', 'CGACT', 'AGACA', 'AAT', 'TTG', 'AAA', 'GAT', 'CGCGA', 'TAA', 'CCTGC', 'TCGAG'], 1: ['CGAAA', 'GCG', 'ATGTC', 'TGC', 'CGAGC', 'CCTAG', 'CAAAA', 'TGT', 'AGC', 'TGCGA', 'CTA', 'ACG', 'TCAGC', 'ACC', 'TAGAC', 'TCA', 'TCACC', 'ATA', 'ATCCA', 'GCT', 'CCA', 'TCG', 'GTA', 'GTCGC', 'GGATC', 'GCA', 'GCTTA', 'AAGCG', 'AGT', 'GTCAG', 'GCATA', 'TAT', 'TTT', 'GTGAG', 'GTCCT', 'TTA', 'GTCCA', 'GAG', 'ATT', 'TCCTA', 'CGTGT', 'GGAGT', 'CCC', 'GCCAA', 'TAAGC', 'CAG', 'TCAAG', 'AAT', 'TCT', 'CGT', 'ACACC', 'GGT', 'CAT', 'AAA', 'TCCAA', 'CGTCG', 'GCC', 'GGC', 'GTC', 'GAC', 'GAA', 'GACCT', 'TAA', 'TGAGC', 'TTC', 'GAAAA', 'AAG', 'CCAAG', 'ATATC', 'ATC', 'TGTAA', 'GTGTT'], 2: ['CGAAA', 'GCG', 'TGC', 'ATGTG', 'GTG', 'TGT', 'AGGGC', 'TTCAA', 'CAAGT', 'ACCAA', 'CGA', 'GGG', 'GTAAG', 'GGTGT', 'TGAAA', 'TAT', 'TTCGA', 'TTA', 'ATT', 'TCAAG', 'TCCAA', 'GCC', 'GAA', 'GAAAA', 'AAG', 'CCAAG', 'CAAAG', 'CAA', 'GCAGT', 'ATCGT', 'AAAGC', 'AGC', 'AAAAC', 'TAG', 'TAAAG', 'ACG', 'CGC', 'TCA', 'ATA', 'ATGTA', 'GCT', 'CAAGG', 'TCG', 'TTTCG', 'GTA', 'GTCGA', 'CCCAA', 'GCA', 'GTTAG', 'AGT', 'AAGGG', 'CGAGT', 'CTAGA', 'CCAGG', 'GAG', 'AGTGA', 'AAT', 'CGT', 'GAAAG', 'GGT', 'AAA', 'TAA', 'ATTCA', 'CCAGT', 'AAACG', 'TCGAG', 'GTGTT'], 3: ['GCG', 'TAAGA', 'AGC', 'TGAAG', 'AAGCT', 'TGCCC', 'ACG', 'TGCTC', 'CGC', 'TCA', 'CTG', 'ATAAG', 'AGA', 'ATA', 'CATGA', 'TTAGA', 'GCT', 'AAGGT', 'GTAAG', 'GAAGA', 'TAGTC', 'AAGAT', 'GAAGT', 'GCTAG', 'AAC', 'TGCGC', 'CGCTC', 'TGGTC', 'CCG', 'AAGTT', 'CACGG', 'TAT', 'TTT', 'AGTCT', 'CGCAC', 'CGG', 'GAAGC', 'ATT', 'GAAGG', 'TCAGA', 'CCC', 'GGTTA', 'CAG', 'TTAAA', 'AAT', 'TCT', 'CGT', 'CATAT', 'GGT', 'AAA', 'GCC', 'GAC', 'GTC', 'TTCCC', 'TAA', 'AAG', 'CAA', 'GTAGT', 'GGAAG', 'GCTCA'], 4: ['GGA', 'ATTTA', 'GTG', 'GCTCC', 'TTAAG', 'CGA', 'AATCC', 'CCA', 'TTGAG', 'AAC', 'AGTCG', 'CGTCC', 'ACATC', 'AGTAG', 'CCG', 'TAT', 'TTA', 'ATT', 'AGG', 'ACCGA', 'CCCGG', 'CAT', 'GCGAC', 'ACCTA', 'GCC', 'GACCT', 'GTGGC', 'ATTCC', 'TAC', 'AAG', 'CCATG', 'TTAGT', 'CAA', 'GTAGG', 'CGAGC', 'ACGTC', 'AGC', 'ACAGG', 'TCC', 'AGA', 'ATA', 'GACCG', 'TCG', 'CGTCA', 'GTA', 'CCCAA', 'CCGAA', 'AATGG', 'GCA', 'TAATG', 'TTTAA', 'GAACG', 'TTT', 'CCAGG', 'GAG', 'CGG', 'ACCCA', 'CAG', 'TTAAA', 'AAT', 'CGT', 'CGGGC', 'AAA', 'CGATT', 'TGTAG', 'ATAGT', 'CTCCG', 'AGAAC', 'TAA'], 5: ['CTGCT', 'TGC', 'TCCAT', 'CTT', 'TGG', 'GTG', 'ATG', 'AAAAC', 'TAG', 'AGATT', 'GTT', 'ACG', 'TCC', 'AGAGC', 'ATGAC', 'GTAGC', 'TTCAG', 'CGC', 'CACGC', 'TAGAC', 'CTG', 'CGA', 'ATA', 'GCT', 'ACAAC', 'CGTGG', 'AACGC', 'GCACG', 'CAAGA', 'ACAAG', 'GCA', 'CTC', 'GTACC', 'AGT', 'CCG', 'TAT', 'TCTGG', 'CCGAG', 'TTA', 'CTAGA', 'AACGT', 'CGG', 'GTTCC', 'ATT', 'CGCGC', 'GCCGT', 'ACGTA', 'CTCGA', 'AAT', 'CGT', 'TTG', 'GGT', 'CAT', 'GCC', 'GGC', 'GTC', 'CTCCG', 'GAA', 'GAT', 'TAA', 'GACGT', 'TAC', 'AAG', 'TCAGG', 'CAAAG', 'CAA', 'GCAGT', 'GTAGT']}}
	features_list = {}
	with open(feature_list_file, 'r') as fp:
	    features_list = json.load(fp)
	nuc_start_position = 41
	amino_start_position = 13
	for block_index in range(len(nucleo_block_length)):
		final_k_mer_dict = {}
		for i, k in enumerate(nucleo_k_values):
			nucleo_k_value = k
			nuc_end_position = nuc_start_position + nucleo_block_length[block_index]
			nuc_keys = list(map(''.join, product('ACTG', repeat=int(nucleo_k_value))))
			#get the specific feature list for each nucleotide k value
			nuc_features = features_list['nuc'][str(block_index)]
			nuc_features = [item for item in nuc_features if len(item) is int(nucleo_k_value)]
			nuc_features.sort()

			prep_dict = get_k_mer_list(nucleo_k_value, files, nuc_keys, sequence_directory, nuc_start_position, nuc_end_position, nuc_features)
			if i == 0:
				final_k_mer_dict = prep_dict
			else:
				for each_seq_final in final_k_mer_dict:
					for each_seq_prep in prep_dict:
						if each_seq_final['pid'] == each_seq_prep['pid']:
							each_seq_final['features_names']['NU'] = each_seq_final['features_names']['NU'] + (each_seq_prep['features_names']['NU'])
							each_seq_final['features']['NU'].extend(each_seq_prep['features']['NU'])
							break
						else:
							continue
		"done nucleotide"
		for i, k in enumerate(amino_k_values):
			amino_k_value = k
			amino_end_position = amino_start_position + amino_block_length[block_index]
			amino_keys = get_key_product(amino_k_value, files, amino_directory, amino_start_position, amino_end_position)
			#get the specific feature list for each amino k value
			amino_features = features_list['amino'][str(block_index)]
			amino_features = [item for item in amino_features if len(item) == int(amino_k_value)]
			amino_features.sort()

			prep_dict = get_k_mer_list(amino_k_value, files, amino_keys, amino_directory, amino_start_position, amino_end_position, amino_features)
			for each_seq_final in final_k_mer_dict:
				for each_seq_prep in prep_dict:
					if each_seq_final['pid'] == each_seq_prep['pid']:
						if i == 0:
							each_seq_final['features']['AA'] = each_seq_prep['features']['AA']
							each_seq_final['features_names']['AA'] = each_seq_prep['features_names']['AA']
						else:
							each_seq_final['features']['AA'] = each_seq_final['features']['AA'] + each_seq_prep['features']['AA']
							each_seq_final['features_names']['AA'].extend(each_seq_prep['features_names']['AA'])
						break
					else:
						continue
		"done amino"
		nuc_start_position = nuc_end_position
		amino_start_position = amino_end_position
		file_str = ""
		block_name=str(block_index)
		for index, file in enumerate(files):
			temp = file.replace(".fasta", "")
			file_str += temp
		save_as_json(final_k_mer_dict, "feature_data/" + block_name + 'block-'+ amino_k_values + nucleo_k_values + "-mer-"+ file_str + ".json")




