**Sequence Classification Project**

## How to run the new files
### step 1:

### step 2:

### step 3: split_train_test.py
 * Decides which data will be part of the train set and the test set.
 * Inputs: one or more json features_files, the name of the output_file
 * Output: The same data contained in the features file iput, but with a extra filed, called type that will indicate if the data belongs to train set ou test set.
  * Example command:

  ```
      python split_train_test.py -f 5block-235-mer-coleopteradipterahymenopteralepidoptera.json -o 5block-235-mer-coleopteradipterahymenopteralepidoptera_splited.json
  ```

### step 4: trainer.py
* Generate the trained model
* Inputs: one or more json features_files, the name of the output_file, and the target label
* Output: The trained model saved in a tflearn file.
* Example command:
```
  python3 trainer.py -f 0block-235-mer-coleopteradipterahymenopteralepidoptera_splited.json -o model_lepidoptera_block -t lepidoptera
```
Or

```
  python trainer.py -f 0block-235-mer-coleopteradipterahymenopteralepidoptera_splited.json 1block-235-mer-coleopteradipterahymenopteralepidoptera_splited.json 2block-235-mer-coleopteradipterahymenopteralepidoptera_splited.json 3block-235-mer-coleopteradipterahymenopteralepidoptera_splited.json 4block-235-mer-coleopteradipterahymenopteralepidoptera_splited.json 5block-235-mer-coleopteradipterahymenopteralepidoptera_splited.json -o model_lepidoptera_block -t lepidoptera
```

### step 5: classify.py
* Generate predictions for the test data
* Inputs: one or more json features_files, the name of the output_file, the target label, and the model file
* Outputs: The csv file with all predictions and the real label
* Example command:
```
  python3 classify.py -f 0block-235-mer-coleopteradipterahymenopteralepidoptera_splited.json -m model/model_lepidoptera_block_0.tflearn -o prediction_lepidoptera_block -t lepidoptera
```
Or
```
  python3 classify.py -f 0block-235-mer-coleopteradipterahymenopteralepidoptera_splited.json 1block-235-mer-coleopteradipterahymenopteralepidoptera_splited.json 2block-235-mer-coleopteradipterahymenopteralepidoptera_splited.json 3block-235-mer-coleopteradipterahymenopteralepidoptera_splited.json 4block-235-mer-coleopteradipterahymenopteralepidoptera_splited.json 5block-235-mer-coleopteradipterahymenopteralepidoptera_splited.json -m model/model_lepidoptera_block_0.tflearn model/model_lepidoptera_block_1.tflearn  model/model_lepidoptera_block_2.tflearn  model/model_lepidoptera_block_3.tflearn  model/model_lepidoptera_block_4.tflearn  model/model_lepidoptera_block_5.tflearn -o prediction_lepidoptera_block -t lepidoptera
```
--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

## How to run the old files
### step 1: feature_generation.py
  * This python script is to generate feature data from raw fasta data in data folder.
  * `python feature_generation.py [amino acid k-value] [nucleotide k-value] [orde.fasta-order.fasta] [number of selected features per order in each k-value]`
  * example command: `python feature_generation.py 2 35 Coleoptera.fasta-Diptera.fasta 20`
This script will take coleoptera.fasta and coleoptera.fasta as input data, and use amino acid dimer, k-mer of 3 and 5 for nucleotide as vectors. Finally, it will take 20 best features from all the vectors for each order and each k-value. 
  * output: the output file will be stored in the feature_data folder
```
[[process id], [frequency data], [class]]
```
  * example output: 
```
[[['CNTIB1116-15'], [0.0054347826, 0.0054347826 ... 0.001814882, 0.001814882], [1, 0]]]
```

### step 2: test_train\_split.py
  * This python script is to generate test/training data from feature data. It takes the file in feature_data folder which has been generated from step 1 as an input. One more additional input needs to be added, which is the percent of data that will be used as training data.
  * `python test_train_split.py [file name] [percent of training data]`
  * example command: `python test_train_split.py 235-mer-top-20ColeopteraHymenoptera-ratio.pkl 0.6`
0.6 implies that 60% of data are going to be used as training data.
  * output: the output file will be stored in the train_and_test folder.
```
[[process id], [frequency data], [class], [test/train]]
```
  * example output: 
```
[[['CNTIB1116-15'], [0.0054347826, 0.0054347826 ... 0.001814882, 0.001814882], [1, 0], ['test']]]
```


### step 3: subsampling.py
  * This python script is to take sub-sample of the data. It takes the file in train_and_test folder which has been generated from step 2 as an input. One more additional input needs to be added, which is the percent of data that will be reduced.
  * `python subsampling.py [file_name] [percent of reudced datas]`
  * example command: `python subsampling.py tt-0.6-235-mer-top-20ColeopteraDiptera-ratio.pkl 0.2`
0.6 implies that 20% of train and testing data are going to be taken out.
  * output: The data structure will be the same in step 2. The output file will be stored in the sample_data folder

### step 4: train.py
  * This python script trains the data generated in the previous step.
  * Input: There are three inputs: 
    * --input: The name of the sample file generated in the previous step, example: `-i train_and_test/tt-0.1-single_classifier_diptera.pkl`
    * --classe1: The first class (order), example: `-c1 diptera`
    * --classe2: The second class (order), example: `-c2 not_diptera`
    * --type: type of NN architecture to use, example: `-t 2` (uses architecture 2)
  * Outupt: The generated trained model that will be stored in the model folder.
  * Example command:

  ```
      python train.py -i train_and_test/tt-0.1-single_classifier_diptera.pkl -c1 diptera -c2 not_diptera -t 2
  ```

### step 5: test.py
  * This script predicts the class for the test data.
  * Input: There are four inputs:
    * --input: The name of the sample file generated in the step number 3, example: `-i train_and_test/tt-0.1-single_classifier_diptera.pkl`
    * --classe1: The first class (order), example: `-c1 diptera`
    * --classe2: The second class (order), example: `-c2 not_diptera`
    * --model: The model generated in the previous step, example: `-m diptera_2.tflearn`
    * --type: Type of NN architecture to use, example: `-t 2` (uses architecture 2)
  * Output: The generated file with the predictions will be stored inside the folder output_data.
  * Example command:

  ```
      python test.py -i train_and_test/tt-0.1-single_classifier_diptera.pkl -c1 diptera -c2 not_diptera -m diptera_2.tflearn -t 2
  ```

### step 6: metrics.py
  * This script gives metrics related to the performance of the Deep Neural Network.
  * Input: There are four inputs:
    * --input: The name of the sample file generated in the step number 3, example: `-i train_and_test/tt-0.1-single_classifier_diptera.pkl`
    * --classe1: The first class (order), example: `-c1 diptera`
    * --classe2: The second class (order), example: `-c2 not_diptera`
    * --p: The name of the file the contains the predictions, generated in the previous step., example: `-p predictions_diptera_not_diptera.tsv`
  * Outputs: 
      * The generated file with the predictions will be stored inside the folder output_data.
      * A file containing the data the was misclassified, this data will be stored inside the folder output_data.
  * Example command:

  ```
  python metrics.py -i train_and_test/tt-0.1-single_classifier_diptera.pkl -c1 diptera -c2 not_diptera -p predictions_diptera_not_diptera.tsv
  ```